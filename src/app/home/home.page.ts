import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  phoneNo: string;
  msg: string;

  constructor() {}

  async sendSMS() {
    const apiKey = 'apikey=' + 'qaNQ1cKUxJo-2MCvuY2EvPwNfYTbWYS3KIqBzgq61E';
    const message = '&message=' + this.msg;
    const sender = '&sender=' + 'TXTLCL';
    const numbers = '&numbers=' + '91' + this.phoneNo;
    const url = encodeURI('https://api.textlocal.in/send/?' + apiKey + sender + numbers + message);

    // const url = encodeURI('https://api.textlocal.in/send/?' + apiKey + numbers + message + sender + '&test=1');
    const response = await fetch(url, {
      method : 'get',
      mode: 'no-cors',
      cache: 'no-store'
    } );
    console.log(response);
    // const jsonResponse = await response.json();
    // console.log('response::', jsonResponse);
  }

}
